#!/bin/bash

#check if a backup exists
if [ ! -e /etc/default/grub ]; then
	sudo cp /etc/default/grub /etc/default/grubBackup
fi

#add nouveau.modeset=0 to grub defaults
sudo sed 's/GRUB_CMDLINE_LINUX_DEFAULT=\"/GRUB_CMDLINE_LINUX_DEFAULT=\"nouveau.modeset=0 /' /etc/default/grub > /etc/default/almostGrub

sudo rm -f /etc/default/grub
sudo mv /etc/default/almostGrub /etc/default/grub
sudo grub-mkconfig -o /boot/grub/grub.cfg
