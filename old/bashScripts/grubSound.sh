#!/bin/bash

#make a backup
sudo cp /etc/default/grub /etc/default/grubBackup

#add nouveau.modeset=0 to grub defaults
sudo sed "s/GRUB_CMDLINE_LINUX_DEFAULT=\"/GRUB_CMDLINE_LINUX_DEFAULT=\"acpi_osi=\'!Windows 2013\' acpi_osi=\'!Windows 2012\' acpi_backlight=vendor acpi_backlight=video video.use_native_backlight=1 /" /etc/default/grub > /etc/default/almostGrub

sudo rm -f /etc/default/grub
sudo mv /etc/default/almostGrub /etc/default/grub
sudo grub-mkconfig -o /boot/grub/grub.cfg
