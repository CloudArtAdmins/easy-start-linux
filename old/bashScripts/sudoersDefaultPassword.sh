#!/bin/bash

sudo cat /etc/sudoers | grep rootpw > TEMP
	
#if not already there
cat TEMP
if [ ! -s TEMP ] ; then
	#add it in
	echo "Defaults        rootpw" | sudo EDITOR='tee -a' visudo 
else
	echo "Root password is already the default."
fi

#remove temp
sudo rm -f TEMP
